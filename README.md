# ternary-rs

A Rust library for emulating the ternary operator from C/C++ (among other languages)

Exposes two macros that do the same thing:

-   Choose between two macros `ifelse!` and `ternary`

```rust
ifelse!(condition, true value, false value)
ternary!(condition, true value, false value)
```

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
ternary-rs = "1.0.0"
```

## Example

```rust
let cond = true;
let result = if_else!(cond, 1, 0);
assert_eq!(1, result);
assert_ne!(0, result);

// If you find if_else!() unclear, you can use the ternary!() macro and be explicit instead!
let result = ternary!(cond, 1, 0);
assert_eq!(1, result);
assert_ne!(0, result);
```

## License
ternary-rs is distributed under the terms of both the MIT license and the
Apache License (Version 2.0).

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) for details.