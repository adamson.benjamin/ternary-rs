#[macro_export]
macro_rules! if_else {
    ($c:expr, $t:expr, $f:expr) => {
        if $c {
            $t
        } else {
            $f
        }
    };
}

#[macro_export]
macro_rules! ternary {
    ($c:expr, $t:expr, $f:expr) => {
        if_else!($c, $t, $f)
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn ifelse_works() {
        assert_eq!(7, if_else!(true, 7, 11));
        assert_eq!(11, if_else!(false, 7, 11));

        assert_eq!(11, if_else!(false, 7, 11));
        assert_ne!(7, if_else!(false, 7, 11));
    }

    #[test]
    fn ternary_works() {
        assert_eq!(7, ternary!(true, 7, 11));
        assert_eq!(11, ternary!(false, 7, 11));

        assert_eq!(11, ternary!(false, 7, 11));
        assert_ne!(7, ternary!(false, 7, 11));
    }
}
